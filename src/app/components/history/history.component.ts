import { Component, OnInit } from '@angular/core';
import {Card} from '../../model/card';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {UserService} from '../../services/user.service';
import {Subject} from 'rxjs';
import {Game} from '../../model/game';
import {Player} from '../../model/player';
import {filter} from 'rxjs/operators';
import {log} from 'util';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  gameId: string;
  turnCount = 0;
  history: History;
  gameSubject: Subject<Game> = null;
  game: Game;
  time = 1000;
  interval;
  isPlaying = false;
  hasNext = false;

  constructor(private route: ActivatedRoute, private http: HttpClient, private userService: UserService) {
  }

  ngOnInit() {
    this.gameId = this.route.snapshot.paramMap.get('id');
    this.getHistory();
  }

  getHistory() {
    this.http.get(environment.api_url + 'history?game=' + this.gameId, this.userService.httpOptions)
      .subscribe((next: History) => {
        this.history = next;
        this.start();
      });
  }

  clickPlay() {
    this.isPlaying = true;
    this.interval = setInterval(() => {
      this.next();
    }, this.time);
  }

  clickPause() {
    this.isPlaying = false;
    clearInterval(this.interval);
  }

  start() {
    this.game = new Game(Number(this.gameId), this.history.players, this.history.card);
    this.game.color = null;
    this.gameSubject = new Subject();
    this.hasNext = this.history.turns.length > 0;
  }

  initial() {
    this.gameSubject.next(this.game);
  }

  next() {
    if (this.turnCount >= this.history.turns.length) {
      this.hasNext = false;
      return;
    }
    const turn = this.history.turns[this.turnCount];
    if (turn.card != null) {
      if (turn.action === 'PLAY') {
        this.game.topCard = turn.card;
        this.game.players.forEach(player => {
          player.turn = false;
          if (player.userId === turn.player) {
            player.amount--;
            player.turn = true;
            if (player.cards != null) {
              player.cards.slice(player.cards.indexOf(turn.card), 1);
            }
          }
        });
      } else {
        this.game.players.forEach(player => {
          player.turn = false;
          if (player.userId === turn.player) {
            player.amount++;
            player.cards.push(turn.card);
            player.turn = true;
          }
        });
      }
    } else {
      this.game.players.forEach(player => {
        player.turn = false;
        if (player.userId === turn.player) {
         player.amount++;
         player.turn = true;
        }
      });
    }
    this.gameSubject.next(this.game);
    this.turnCount++;
  }
}
class History {
  public card: Card;
  public players: Player[];
  public turns: Turn[];
}
class Turn {
  public card: Card;
  public action: string;
  public player: number;
}
