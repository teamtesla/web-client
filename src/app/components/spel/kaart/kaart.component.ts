import {Component, Input, OnInit} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-kaart',
  templateUrl: './kaart.component.html',
  styleUrls: ['./kaart.component.scss']
})
export class KaartComponent implements OnInit {
  public _cardName: string;
  @Input() public choiceColor: string;

  constructor(private sanitizer:DomSanitizer) {
  }
  @Input()
  set cardName(name: string) {
    this._cardName = name;
    this.setImage();
  }
  setImage(){
    if(this._cardName == "WD4.png"){
      if (this.choiceColor == "GREEN") this._cardName = "GD4.png";
      if (this.choiceColor == "YELLOW") this._cardName = "YD4.png";
      if (this.choiceColor == "BLUE") this._cardName = "BD4.png";
      if (this.choiceColor == "RED") this._cardName = "RD4.png";
    }
    if(this._cardName == "WC.png"){
      if (this.choiceColor == "GREEN") this._cardName = "GC.png";
      if (this.choiceColor == "YELLOW") this._cardName = "YC.png";
      if (this.choiceColor == "BLUE") this._cardName = "BC.png";
      if (this.choiceColor == "RED") this._cardName = "RC.png";
    }
    console.log("KAAART KLEUUUR");
  }
  ngOnInit() {
  }
  plusvier(){
    if(this._cardName == "WD4.png") return true;
    if(this._cardName == "WC.png") return true;
    return false;
  }
  reverse(){
    if(this._cardName == "RR.png") return true;
    if(this._cardName == "BR.png") return true;
    if(this._cardName == "YR.png") return true;
    if(this._cardName == "GR.png") return true;
    return false;
  }
  plus(){
    if(this._cardName == "RD2.png") return true;
    if(this._cardName == "BD2.png") return true;
    if(this._cardName == "YD2.png") return true;
    if(this._cardName == "GD2.png") return true;
    return false;
  }
  block(){
    if(this._cardName == "RS.png") return true;
    if(this._cardName == "BS.png") return true;
    if(this._cardName == "YS.png") return true;
    if(this._cardName == "GS.png") return true;
    return false;
  }
  getChoiceColor(){
    if(this._cardName.startsWith('W')){
    var c : string;
    if(this.choiceColor == "GREEN") c = "Green";
    else if(this.choiceColor == "RED") c = "Red";
    else if(this.choiceColor == "BLUE") c = "Blue";
    else if(this.choiceColor == "YELLOW") c = "Yellow";
    else return "";

    return this.sanitizer.bypassSecurityTrustStyle("1px solid " + c);
    }
    return "";
  }

}
