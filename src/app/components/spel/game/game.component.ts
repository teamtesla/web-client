
import { Component, OnInit, SystemJsNgModuleLoader, Sanitizer, Input, Output, EventEmitter } from '@angular/core';
import { Player } from '../../../model/player';
import { Card, Color } from '../../../model/card';
import { UserService } from '../../../services/user.service';
import { User } from '../../../model/user';
import { ActivatedRoute } from '@angular/router';
import { GameService } from '../../../services/game.service';
import { Game } from '../../../model/game';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ChooseColorComponent } from '../choose-color/choose-color.component';
import { resultList, RxSpeechRecognitionService } from '@kamiazya/ngx-speech-recognition';
import { DomSanitizer } from '@angular/platform-browser';
import { Subject, Observable } from 'rxjs';
import { LogService, LogType } from '../../../services/log.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  @Input() inHistory = false;
  @Input() gameSubject: Subject<Game>;
  @Output() ready: EventEmitter<any> = new EventEmitter();
  private gameObservable: Observable<Game>;
  private gameId: number;
  public currentCard: Card;
  public players: Player[] = [null, new Player(null, null, null, null, null), null, null, null, null, null, null, null];
  private deck: Card[] = [];
  public player: Player;
  private uno: boolean;
  private first = true;
  private winner : string = null;
  private cardplusSubject: Subject<number>[] = [new Subject<number>(),new Subject<number>(),new Subject<number>(),new Subject<number>(),new Subject<number>(),new Subject<number>(),new Subject<number>(),new Subject<number>()];

  constructor(
    private userService: UserService,
    private gameService: GameService,
    private dialog: MatDialog,
    private logger: LogService,
    private sanitizer: DomSanitizer,
    private SpeechService:RxSpeechRecognitionService) {
  }

  ngOnInit() {
    if (this.inHistory) {
      this.gameObservable = this.gameSubject.asObservable();
    } else {
      this.gameObservable = this.gameService.getGameObservable();
    }
    const _this = this;
    this.gameObservable.subscribe(function (x) {
      _this.assignCurrentCard(x.topCard.color, x.topCard.value, x.topCard.cardId, x.color);

      _this.logger.log("TURN RECEIVED " + JSON.stringify(x), 2, LogType.special, _this);

      let game = x;
      _this.gameId = game.gameId;
      _this.uno = false;

      const tempPlayerDeck: Card[] = [];
      x.players.find(playerX => playerX.cards != null).cards.forEach(card => {
        tempPlayerDeck.push(new Card(card.color, card.value, card.cardId));
      });

      _this.currentCard.ranrot = Math.random() * 90 - 45;
      _this.currentCard.ranX = Math.random() * 30 - 15;
      _this.currentCard.ranY = Math.random() * 30 - 15;

      //check if received top card is new card, if so add card to deck
      if (_this.deck.length == 0) _this.deck.push(_this.currentCard);
      else {
        _this.logger.log("DECK SIZE:" + _this.deck.length, 1, LogType.special, _this)
      }
      if (_this.currentCard.cardId != _this.deck[_this.deck.length - 1].cardId) {
        _this.deck.push(_this.currentCard);
      }
      else {
        _this.logger.log("SAME CARD" + JSON.stringify(_this.currentCard) + " | " + JSON.stringify(_this.deck), 1, LogType.special, _this)
      }

      /// if (x.players.find(playerX => playerX.cards != null).cards.length != tempPlayerDeck.length || _this.first == true) {
        // x.players.find(playerX => playerX.cards != null).cards = tempPlayerDeck;
        _this.first = false;



        // Calculate pluscards
        var pluscards : UserPlusCard[] = [];
        for (let index = 0; index < _this.players.length; index++) {
          var elementA : Player = _this.players[index];
          if(elementA != null){
          var elementB : Player = x.players[index];


          var result = elementB.amount - elementA.amount;
          var pc : UserPlusCard = {cardCount:result,userId:index};
          if(result>0)
          pluscards.push(pc);
          }
        }
        pluscards.forEach(pc => {
          _this.cardplusSubject[pc.userId].next(pc.cardCount);
        });

        _this.players = x.players;
        const playerWithCards = x.players.find(playerX => playerX.cards != null);
        _this.player = new Player(playerWithCards.userId, tempPlayerDeck.length, tempPlayerDeck, playerWithCards.turn, playerWithCards.username);
      _this.players = x.players;
      
      //CHECK WIN
      if(_this.players.find(p=>p.amount == 0)){
        _this.winner = _this.players.find(p=>p.amount == 0).username;
      }
      _this.checkUno();
    });
    this.gameService.initializeGame();
    this.ready.emit();
  }

    

   private checkUno() {
     if (this.inHistory) {
      return;
     }
     let message: string;
     if (this.player.cards.length === 2) {
      this.SpeechService.listen()
         .pipe(resultList)
         .subscribe((list: SpeechRecognitionResultList) => {
           message = list.item(0).item(0).transcript;
           if (message === 'Uno') {
             this.uno = true;
           }
         });
     }
   }

  public clickUno() {
    if (this.inHistory) {
      return;
    }
    if (this.player.cards.length === 2) {
      this.uno = true;
    }
  }

  private assignCurrentCard(color: string, value: string, id: number, choice: string) {
    this.currentCard = new Card(color, value, id);
    this.currentCard.choiceColor = choice;
  }

  /*
    setTestUsers() {
      const deck0: Card[] = new Array();
      for (let i = 0; i < 7; i++) {
        deck0.push(new Card(this.drawRandomCard()));
      }
      this.users.push(new Speler(0, new User( 'user0', 'AbC321dE', 'fake@test.mail'), deck0));

      const deck1: Card[] = new Array();
      for (let i = 0; i < 7; i++) {
        deck1.push(new Card(this.drawRandomCard()));
      }
      this.users.push(new Speler(1, new User('user1', 'AbC325dE', 'fake2@test.mail'), deck1));
    private drawCardToStack(card: Card) {
      this.deck.push(card);
    }
  */
  private assignTurnToUser(userId: number) {
    this.players.find(user => user.userId === userId).turn = true;
  }

  onCardClicked(card: Card) {
    if (this.inHistory) {
      return;
    }
    if (this.player.turn) {
      if (this.isClickedCardPossible(card) && !this.isColorNeeded(card)) {
        this.currentCard = card;
        this.currentCard.ranrot = Math.random() * 90 - 45;
        this.currentCard.ranX = Math.random() * 30 - 15;
        this.currentCard.ranY = Math.random() * 30 - 15;
        this.deck.push(card);

        //remove played card from hand
        var index = this.player.cards.indexOf(card);
        if (index > -1) {
          this.player.cards.splice(index, 1);
        }


        this.gameService.sendGameData(this.gameId, card.cardId, this.player.userId, this.uno);
      } else if (this.isColorNeeded(card)) {
        this.sendColorCard(card);
      }
    }
  }

  takeCard() {
    if (this.inHistory) {
      return;
    }
    if (this.player.turn) {
      this.gameService.sendGameData(this.gameId, 999, this.player.userId, this.uno);
    }
  }

  sendColorCard(card: Card) {
    const dialogRef = this.dialog.open(ChooseColorComponent, {
      height: '400px',
      width: '600px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.gameService.sendGameDataWithColor(this.gameId, card.cardId, this.player.userId, false, result);
    });
  }

  isClickedCardPossible(card: Card): boolean {
    const lastCard = this.deck[this.deck.length - 1];
    if ((card.color as Color === Color.Wild) || (card.color === lastCard.color) || card.number === lastCard.number) {
      return true;
    } else {
      return Color.Wild === lastCard.color as Color;
    }
  }

  isColorNeeded(card: Card): boolean {
    return card.image === 'WD4' || card.image === 'WC';
  }
  getSafeTransform(card : Card){
    return this.sanitizer.bypassSecurityTrustStyle("rotate(" + card.ranrot +"deg) translate(" + card.ranX + "px," + card.ranY + "px)");
  }
}
class UserPlusCard{
  userId:number;
  cardCount:number;
}
