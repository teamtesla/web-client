import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GameComponent} from './game.component';
import {PlayerComponent} from '../player/player.component';
import {HandComponent} from '../hand/hand.component';
import {KaartComponent} from '../kaart/kaart.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {StorageServiceModule} from 'angular-webstorage-service';
import {MatDialogModule} from '@angular/material';
import {RxSpeechRecognitionService} from '@kamiazya/ngx-speech-recognition';
import {RouterTestingModule} from '@angular/router/testing';

describe('GameComponent', () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GameComponent,
        PlayerComponent,
        HandComponent,
        KaartComponent
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      imports: [
        HttpClientTestingModule,
        StorageServiceModule,
        MatDialogModule,
        RouterTestingModule
      ],
      providers: [
        RxSpeechRecognitionService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
