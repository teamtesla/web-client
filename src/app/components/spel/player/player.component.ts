import {Component, Input, OnChanges, OnInit, OnDestroy} from '@angular/core';
import {Player} from '../../../model/player';
import { Observable } from 'rxjs';
import {PictureService} from '../../../services/picture.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-speler',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit, OnChanges {

  constructor(pictureService: PictureService,
    private sanitizer: DomSanitizer) {this.pictureService = pictureService;
  }
  public progress = 0;
  public imgUrl :any  = '/assets/img/unknown-player.png';
  private interval;
  private plusCards: number = null;
  private pictureService: PictureService;
  @Input() public speler: Player;
  @Input() public beurt: boolean;
  @Input() public events : Observable<number>;




  // TODO vervang naam door user.name
  ngOnInit() {
    if(this.speler != null && this.speler.userId != null){
    // TODO check profile picture en indien deze bestaat vervang imgUrl
    var _this = this;
    console.log("REAL PLAYER");
    this.pictureService.getProfilePicture(this.speler.userId).subscribe(result => {_this.imgUrl = _this.sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64, ' + result.imageData); console.log("[IMAGE RECEIVED]" + result.imageData)});
    
    
    this.events.subscribe((x) => {
      _this.plusCards = x;
      setTimeout(function() {
        _this.plusCards = null;
      }, 2000);
    });
  }
}

  ngOnChanges() {
    if (this.progress === 100 || this.progress === 0) {
      if (this.beurt) {
        this.startTimer();
      }
    }
  }

  startTimer() {
    this.progress = 0;
    this.interval = setInterval(() => {
      this.progress++;
      console.log(this.progress);
      if (this.progress === 100) {
        clearInterval(this.interval);
        this.progress = 0;
        this.beurt = false;
      }
    }, 100);
  }

  public startBeurt() {
    this.startTimer();
  }
}
