import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-choose-color',
  templateUrl: './choose-color.component.html',
  styleUrls: ['./choose-color.component.scss']
})
export class ChooseColorComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ChooseColorComponent>) { }

  ngOnInit() {
  }

  public choose(color: string) {
    this.dialogRef.close(color);
  }
}
