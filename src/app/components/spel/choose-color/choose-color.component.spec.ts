import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChooseColorComponent} from './choose-color.component';
import {MatDialogModule, MatDialogRef} from '@angular/material';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ChooseColorComponent', () => {
  let component: ChooseColorComponent;
  let fixture: ComponentFixture<ChooseColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ChooseColorComponent
      ],
      imports: [
        MatDialogModule
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {}
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
