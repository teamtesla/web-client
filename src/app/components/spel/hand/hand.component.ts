import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Card, Color} from 'src/app/model/card';

@Component({
  selector: 'app-hand',
  templateUrl: './hand.component.html',
  styleUrls: ['./hand.component.scss']
})
export class HandComponent implements OnInit, OnChanges {

  @Input() deck: Card[] = [];
  selectedCard = -1;
  playedCard = 'You haven\'t played a card yet.';
  handWidth;
  handHeight: number;
  windowWidth;
  @Output() clickedCard = new EventEmitter<Card>();

  constructor() {
    this.handHeight = 486;
    this.windowWidth = window.innerWidth;
    this.generateCards();
    this.sortCards();
    this.positionCards();
  }

  ngOnInit() {

  }

  ngOnChanges() {
    this.sortCards()
    this.positionCards();
  }

  playCard(c: Card) {
    this.clickedCard.emit(c)
    this.positionCards();
  }

  drawCard() {
    this.drawValidCard();
    this.sortCards();
    this.positionCards();
  }

  positionCards() {
    // this.calcHandWidth();
    let x = 0;
    for (const c of this.deck) {
      c.id = x;
      x++;
    }
  }

  sortCards() {
    this.deck = this.deck.sort((a, b) => {
      if (a.color !== b.color) {
        if (a.color === Color.Blue) {
          return -1;
        }
        if (a.color === Color.Green) {
          if (b.color === Color.Blue) {
            return 1;
          }
          return -1;
        }
        if (a.color === Color.Red) {
          if (b.color === Color.Blue || b.color === Color.Green) {
            return 1;
          }
          return -1;
        }
        if (a.color === Color.Yellow) {
          if (b.color !== Color.Wild) {
            return 1;
          }
          return -1;
        }
        if (a.color === Color.Wild) {
          return 1;
        }

      }
      return a.number - b.number;
    });
    this.deck.forEach((c) => {
    });
  }

  getBackground(id: number) {
    return 'url(\'assets/img/cards/' + this.deck[id].image + '.png\')';
  }

  resize() {
    this.windowWidth = window.innerWidth;
    // this.calcHandWidth();
    // this.calcHandHeight();
  }

  /*calcHandWidth() {
    if (this.deck.length * this.handHeight > this.windowWidth - this.handHeight) {
      this.handWidth = this.windowWidth - this.handHeight;
    } else {
      this.handWidth = this.deck.length * this.handHeight;
    }
  }*/

  /*calcHandHeight() {
    if (this.deck.length > 0) {
      this.handHeight =  486 / 759 * $('#Hand').height();
    }
  }*/

  generateCards() {
    for (let i = 0; i < 30; i++) {
      this.drawValidCard();
    }
  }


  drawValidCard() {
    if (this.deck.length === 108) {
      throw Error('You literally have the entire deck in your hand...');
    }
  }

  getCount(countCard: Card) {
    return this.deck.filter((card) => countCard.color === card.color && countCard.number === card.number).length;
  }
}
