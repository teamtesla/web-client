import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {PictureService} from '../../services/picture.service';
import {User} from '../../model/user';
@Component({
  selector: 'app-add-picture',
  templateUrl: './add-picture.component.html',
  styleUrls: ['./add-picture.component.scss']
})
export class AddPictureComponent implements OnInit {
  public file: File;
  constructor(private pictureService: PictureService) {
  }

  ngOnInit() {
  }

  onChange(event) {
    this.file = event.target.files[0];
    this.setPicture(this.file);
  }

  private setPicture(image: File) {
    this.pictureService.setProfilePicture(image);
  }

}
