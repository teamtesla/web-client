import { Component, OnInit } from '@angular/core';
import { LobbyService } from 'src/app/services/lobby.service';
import { WebsocketDispatcherService } from 'src/app/services/websocket-dispatcher.service';
import { Lobby } from 'src/app/model/lobby';
import { Router } from '@angular/router';
import {FriendRequest, FriendService} from '../../services/friend.service';
import {log} from 'util';
import {el} from '@angular/platform-browser/testing/src/browser_util';
import {Invite} from '../../model/invite';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  notificationItems: NotificationItem[] = [];
  friendRequests: FriendRequest[] = [];
  lobbyInvites: Invite[] = [];
  constructor(
    private lobbyService: LobbyService,
    private dispatcher: WebsocketDispatcherService,
    private friendService: FriendService,
    private router: Router) { }

  ngOnInit() {
    this.getLobbyInvites();
    this.getFriendRequests();
    this.iterateNotifications();
  }
  getLobbyInvites() {
    this.iterateLobbyInvites(this);
    const _this = this;
    this.lobbyService.lobbyInvites.asObservable().subscribe((x) => {
      _this.iterateLobbyInvites(_this);
      _this.iterateNotifications();
    });
  }
  getFriendRequests() {
    this.friendRequests = this.friendService.lastFriendRequests;
    const _this = this;
    this.friendService.friendRequests.asObservable().subscribe((x) => {
      _this.friendRequests = x;
      _this.iterateNotifications();
    });
  }
  private iterateLobbyInvites(_this) {
    _this.lobbyInvites = [];
    _this.lobbyService.lastLobbyInvites.forEach((x) => {
      _this.lobbyInvites.push(x);
    });
  }
  private iterateNotifications() {
    const _this = this;
    this.notificationItems = [];
    this.lobbyInvites.forEach(invite => {
      const notitem: NotificationItem = new NotificationItem();
      notitem.setSort(NotificationItemSort.lobbyInvite);
      notitem.invite = invite;
      notitem.message = invite.sender.username + ' invites you to join his/her lobby';
      _this.notificationItems.push(notitem);
    });
    this.friendRequests.forEach(request => {
      const notitem: NotificationItem = new NotificationItem();
      notitem.setSort(NotificationItemSort.friendInvite);
      notitem.invite = request;
      notitem.message = request.sender + ' wants to be your friend';
      _this.notificationItems.push(notitem);
    });
  }
  accept(not: NotificationItem) {
    if (not.getSort() === NotificationItemSort.lobbyInvite) {
      this.dispatcher.sendMessage("invitation/remove", not.invite);
      this.joinLobby(not.invite.lobby);
      this.lobbyInvites.slice(this.lobbyInvites.indexOf(not.invite), 1);
    } else {
      this.acceptFriendRequest(not.invite);
    }
  }
  decline(not: NotificationItem) {
    if (not.getSort() === NotificationItemSort.lobbyInvite) {
      this.dispatcher.sendMessage("invitation/remove", not.invite);
      this.lobbyInvites.slice(this.lobbyInvites.indexOf(not.invite), 1);
    } else {
      this.declineFriendRequest(not.invite);
    }
  }
  joinLobby(lobby: number) {
    this.lobbyService.lastCurrentLobby = null;
    this.dispatcher.sendMessage("lobby/change", new LobbyChange(lobby));
    this.router.navigate(["/lobby"]);
  }
  acceptFriendRequest(friendRequest: FriendRequest) {
    this.friendRequests.slice(this.friendRequests.indexOf(friendRequest), 1);
    this.friendService.acceptFriendRequest(friendRequest);
  }
  declineFriendRequest(friendRequest: FriendRequest) {
    this.friendRequests.slice(this.friendRequests.indexOf(friendRequest), 1);
    this.friendService.declineFriendRequest(friendRequest);
  }
}
class LobbyChange{
  id : number;
  constructor(n:number ){
    this.id = n;
  }
}


class NotificationItem{
message:string;
private sort:NotificationItemSort;
invite:any;
color:string;

setSort(sort :NotificationItemSort){
  this.sort = sort;
  if(this.sort == NotificationItemSort.friendInvite) this.color = "#a5daba";
  if(this.sort == NotificationItemSort.lobbyInvite) this.color = "#ffbb56";

}
getSort() : NotificationItemSort{
  return this.sort;
}

}
enum NotificationItemSort{
  friendInvite,
  lobbyInvite
}
