import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {User} from '../../model/user';
import {UserStatistics} from '../../model/user-statistics';
import {NgScrollbar, ScrollToOptions} from 'ngx-scrollbar';
import {WebsocketDispatcherService} from '../../services/websocket-dispatcher.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})

export class StatisticsComponent implements OnInit {
  currentUser: UserStatistics;
  userStats: UserStatistics[] = new Array();
  users: User[] = null;

  @ViewChild(NgScrollbar) chatScrollbar: NgScrollbar;

  constructor(private dispatcher: WebsocketDispatcherService) {
    this.getStats();
    this.getPersonalStats();
  }

  ngOnInit() {
    // this.setDefaultStats();
    // this.getUser();
  }

  getStats() {
    this.dispatcher.subscribeOnPersonalTopic("statistics/users", (x) => {
      this.userStats = JSON.parse(x);
      console.log(this.userStats);
    });
    this.dispatcher.sendMessage("statistics/users", {});
  }

  getPersonalStats() {
    this.dispatcher.subscribeOnPersonalTopic("statistics/getpersonal", (x) => {
      this.currentUser = JSON.parse(x);
    });
    this.dispatcher.sendMessage("statistics/getpersonal", {});
  }

  getSortedStats(sortMode: number): UserStatistics[] {
    let sortedStats: UserStatistics[];
    switch (sortMode) {
      case 0:
        sortedStats = this.userStats.sort(function (a, b) {
          return b.gamesWon - a.gamesWon;
        });
        break;
      case 1:
        sortedStats = this.userStats.sort(function (a, b) {
          return b.pointsEarned - a.pointsEarned;
        });
        break;
      case 2:
        sortedStats = this.userStats.sort(function (a, b) {
          return b.gamesPlayed - a.gamesPlayed;
        });
        break;
    }
    return sortedStats;
  }

  getPlace(sortMode: number, findUser: UserStatistics): number {
    return this.getSortedStats(sortMode).findIndex(us => us.user.id === findUser.user.id) + 1;
  }
}
