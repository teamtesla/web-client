import { Component, OnInit } from '@angular/core';
import {User} from '../../model/user';
import {WebsocketDispatcherService} from '../../services/websocket-dispatcher.service';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {delay} from 'q';
import {Lobby} from '../../model/lobby';
import {LobbyChange} from '../lobbies/lobbies.component';
import {LobbyService} from '../../services/lobby.service';
import {UserStatistics} from '../../model/user-statistics';
import {PictureService} from '../../services/picture.service';
import { CurrentUserService } from '../../services/current-user.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  public currentUser: User = null;
  public lobbies: Lobby[];
  private currentLobby: Lobby = null;
  private statistics: UserStatistics = null;
  public imgUrl = '/assets/img/unknown-player.png';

  constructor(
    private dispatcher: WebsocketDispatcherService,
    private router: Router,
    private lobbyService : LobbyService,
    private currentUserService : CurrentUserService,
    private pictureService : PictureService,
    private sanitizer: DomSanitizer)
    {
    this.currentLobby = lobbyService.getLastCurrentLobby();
    this.currentUser = currentUserService.lastCurrentUser;


    var _this = this;
    this.currentUserService.currentUser.asObservable().subscribe((x)=>{
      _this.currentUser = x;

      if(_this.currentUser != null){
        _this.pictureService.getProfilePicture(_this.currentUser.unoUserId).subscribe(result => {_this.imgUrl = 'data:image/png;base64, ' + result.imageData; console.log("[IMAGE RECEIVED]" + result.imageData)});
      }
    })

    this.getPersonalLobbies();
    this.getStats();
    

  }
  getImgUrl(){
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.imgUrl);
  }

  ngOnInit() {
  }

  getPersonalLobbies() {
    this.lobbies = new Array();
    this.dispatcher.subscribeOnPersonalTopic("lobby/personal", (x) => {
      this.lobbies = JSON.parse(x);
    });
    this.dispatcher.sendMessage("lobby/personal", {});
  }
  joinLobby(lobby:Lobby){
    if(this.currentLobby !=null && this.currentLobby.lobbyId == lobby.lobbyId){
      this.router.navigate(["/lobby"]);
    }
    else{
      this.lobbyService.lastCurrentLobby = null;
      this.dispatcher.sendMessage("lobby/change",new LobbyChange(lobby.lobbyId));
      this.router.navigate(["/lobby"]);
    }
  }
  getStats() {
    this.dispatcher.subscribeOnPersonalTopic("statistics/getpersonal", (x) => {
      this.statistics = JSON.parse(x);
    });
    this.dispatcher.sendMessage("statistics/getpersonal", {});
  }
}
