import { Component, OnInit } from '@angular/core';
import {Lobby} from '../../model/lobby';
import {LogService} from '../../services/log.service';
import {Router} from '@angular/router';
import {WebsocketDispatcherService} from '../../services/websocket-dispatcher.service';
import {LobbyService} from '../../services/lobby.service';

@Component({
  selector: 'app-personallobbies',
  templateUrl: './personallobbies.component.html',
  styleUrls: ['./personallobbies.component.scss']
})
export class PersonallobbiesComponent implements OnInit {
  public lobbies: Lobby[];
  private currentLobby : Lobby = null;


  constructor(private dispatcher : WebsocketDispatcherService, private router : Router, private lobbyService : LobbyService, private logger:LogService) {
    this.currentLobby = lobbyService.getLastCurrentLobby();
  }

  ngOnInit() {
    this.lobbies = new Array();
    this.dispatcher.subscribeOnPersonalTopic("lobby/personal", (x) => {
      this.lobbies = JSON.parse(x);
    });
    this.dispatcher.sendMessage("lobby/personal", {});
  }

}
