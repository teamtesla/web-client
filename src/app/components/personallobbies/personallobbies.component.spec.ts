import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PersonallobbiesComponent} from './personallobbies.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {StorageServiceModule} from 'angular-webstorage-service';
import {RouterTestingModule} from '@angular/router/testing';

describe('PersonallobbiesComponent', () => {
  let component: PersonallobbiesComponent;
  let fixture: ComponentFixture<PersonallobbiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PersonallobbiesComponent
      ],
      imports: [
        HttpClientTestingModule,
        StorageServiceModule,
        RouterTestingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonallobbiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
