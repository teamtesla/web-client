import { Component, OnInit } from '@angular/core';
import { User } from '../../model/user';
import { UserService } from '../../services/user.service';
import { LobbyService } from 'src/app/services/lobby.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public user: User = new User('', '', '');
  public error = '';
  public loggedIn;

  constructor(private userService: UserService) {
    if (this.userService.getJwtToken()) {
      this.loggedIn = true;
    } else {
      this.loggedIn = false;
    }
  }

  ngOnInit() {
  }

  onSubmit() {
    this.error = '';
    this.login(this.user.email, this.user.password);
  }

  login(email: string, password: string) {
    this.userService.doLogin(email, password).subscribe((result) => {
      if (result) {
        this.error = '';
        this.loggedIn = true;
      } else {
        this.error = this.userService.getErrorMessage;
      }

      this.userService.getFromServerUuid().subscribe((result) => {
        this.userService.setUuid(result);

      });

    }, (err) => {
      this.error = err;
    });

  }
  doSubscriptions() {

  }

}
