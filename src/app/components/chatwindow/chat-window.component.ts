import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ChatMessage} from '../../model/chat-message';
import {NgScrollbar, ScrollToOptions} from 'ngx-scrollbar';
import {WebsocketDispatcherService} from '../../services/websocket-dispatcher.service';
import {LobbyService} from '../../services/lobby.service';
import {Lobby} from '../../model/lobby';

@Component({
  selector: 'app-chatwindow',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.scss']
})

export class ChatWindowComponent implements OnInit {

  chatlog: ChatMessage[] = new Array();
  user: string;

  private newMessage: ChatMessage = null;
  private currentLobby: Lobby = null;

  @ViewChild('chatInput') messageField: ElementRef;
  @ViewChild(NgScrollbar) chatScrollbar: NgScrollbar;

  constructor(private dispatcher: WebsocketDispatcherService, private lobbyService: LobbyService) {
    this.currentLobby = lobbyService.getLastCurrentLobby();
    this.receiveMessages();
  }

  sendMessage() {
    console.log(this.newMessage);
    if (this.newMessage.message !== '') {
      this.dispatcher.sendMessage("chat/message", this.newMessage);
      this.chatlog.push(this.newMessage);
    }
    this.receiveMessages();
  }

  receiveMessage() {
    this.dispatcher.subscribeOnPersonalTopic("chat/message", (x) =>{
      this.newMessage = JSON.parse(x);
      console.log("new message " + this.newMessage)
    });
  }

  receiveMessages() {
    this.dispatcher.subscribeOnPersonalTopic("chat/history", (x) => {
      console.log("receiving...")
      console.log(x);
      this.chatlog = JSON.parse(x);
    });
    this.dispatcher.sendMessage("chat/history", {});
  }

  ngOnInit() {
  }
}
