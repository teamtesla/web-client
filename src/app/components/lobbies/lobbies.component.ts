import { Component, OnInit, OnDestroy } from '@angular/core';
import { Lobby } from 'src/app/model/lobby';
import { WebsocketDispatcherService } from 'src/app/services/websocket-dispatcher.service';
import { Router } from '@angular/router';
import { LobbyService } from 'src/app/services/lobby.service';
import { LogService, LogType } from 'src/app/services/log.service';

@Component({
  selector: 'app-lobbies',
  templateUrl: './lobbies.component.html',
  styleUrls: ['./lobbies.component.scss']
})
export class LobbiesComponent implements OnInit {

  public lobbies : Lobby[];
  private currentLobby : Lobby = null;


  constructor(private dispatcher : WebsocketDispatcherService, private router : Router, private lobbyService : LobbyService, private logger:LogService) {
    this.currentLobby = lobbyService.getLastCurrentLobby();

    var _this = this;
    lobbyService.currentLobbyObservable().subscribe(function(x){

      _this.logger.log("CURRENT LOBBY CHANGE",3,LogType.log,_this);
      _this.currentLobby = x;

      _this.logger.log(JSON.stringify(x),4,LogType.log,_this);

    });
    if(this.lobbies == null)
    this.dispatcher.sendMessage("lobby/list",new Object());
   }

  ngOnInit() {
    this.lobbies = new Array();
    this.lobbies = this.lobbyService.getLastLobbyList();
    this.setColors(this);
    var _this = this;
    this.lobbyService.lobbyListObservable().subscribe(function(x){

      _this.logger.log("lobby list updated",1,LogType.log,_this);
      _this.lobbies = x;
      _this.logger.log(JSON.stringify(x),4,LogType.log,_this);
      _this.setColors(_this);
    });
  }
  setColors(_this){
    var count = 0;
    _this.lobbies.forEach(l=>{
    if(count == 0) l.color = "#ED1C24";
    if(count == 1) l.color = "#FFDE00";
    if(count == 2) l.color = "#0095DA";
    if(count == 3) l.color = "#00A651";
    count++;
    if(count == 4) count = 0;
    })
  }

  addLobby() {
    this.lobbyService.lastCurrentLobby = null;
    this.dispatcher.sendMessage("lobby/create",new LobbyCreate("Jan's lobby"));
    this.router.navigate(["/lobby"]);
  }
  joinLobby(lobby:Lobby){
    if(this.currentLobby !=null && this.currentLobby.lobbyId == lobby.lobbyId){
      this.router.navigate(["/lobby"]);
    }
    else{
    this.lobbyService.lastCurrentLobby = null;
    this.dispatcher.sendMessage("lobby/change",new LobbyChange(lobby.lobbyId));
    this.router.navigate(["/lobby"]);
  }
}

}
export class LobbyCreate{
  name : String;
  constructor(n:string ){
    this.name = n;
  }
}
export class LobbyChange{
  id : number;
  constructor(n:number ){
    this.id = n;
  }
}
