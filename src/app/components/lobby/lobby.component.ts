import { Component, OnInit, OnDestroy } from '@angular/core';
import { WebsocketDispatcherService } from '../../services/websocket-dispatcher.service';
import { Lobby } from '../../model/lobby';
import { LobbyService } from '../../services/lobby.service';
import { User } from '../../model/user';
import { Router } from '@angular/router';
import { Game } from '../../model/game';
import { UserStateService } from '../../services/user-state.service';
import { LogService, LogType } from '../../services/log.service';
import { LobbyChange } from '../lobbies/lobbies.component';
import { Alert } from 'selenium-webdriver';
import { NullAstVisitor } from '@angular/compiler';
import { Subscription } from 'rxjs';
import { NotificationService, NotificationSort } from '../../services/notification.service';
import { FriendService } from '../../services/friend.service';
import {UserService} from '../../services/user.service';
import {log} from 'util';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy {

  public inviteDialogOpen = false;
  public currentLobby: Lobby = null;
  private isHost = false;
  public gameList = new Games();
  private players: User[];
  private created: string;
  private subscriptions: Subscription[] = new Array();
  private friends: User[];

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  constructor(
    private dispatcher: WebsocketDispatcherService,
    private lobbyService: LobbyService,
    private router: Router, private stateService: UserStateService,
    private logger: LogService,
    private notifier: NotificationService,
    private friendService: FriendService,
    private userService: UserService) {
    this.currentLobby = lobbyService.getLastCurrentLobby();
    this.subscribeGameList();

    var _this = this;
    var s = lobbyService.currentLobbyObservable().subscribe((x) => {
      if (x.deleted) {
        alert("Your lobby has been deleted.");
        this.router.navigate(["/lobbies"]);

      }
      _this.logger.log("CURRENT LOBBY CHANGE", 3, LogType.log, _this);
      _this.currentLobby = x;

      _this.logger.log(JSON.stringify(x), 3, LogType.log, _this);



    });
    this.subscriptions.push(s);


    //CHECK USER STATUSES
    if (_this.currentLobby != null && _this.currentLobby.unoUsers != null)
      _this.currentLobby.unoUsers.forEach(u => {
        u.alive = stateService.isAlive(u);
        _this.logger.log("USER STATE CHECK: " + JSON.stringify(u) + ": " + u.alive, 4, LogType.log, _this);
      });
    var s2 = _this.stateService.userStateChangerObservable().subscribe(user => {
      if (_this.currentLobby != null && _this.currentLobby.unoUsers != null) {
        _this.currentLobby.unoUsers.forEach(u => {
          if (user.unoUserId === u.unoUserId) u.alive = user.alive;
          _this.logger.log("USER STATE CHECK: " + JSON.stringify(u) + ": " + u.alive, 4, LogType.log, _this);

        });
      }
    });
    this.subscriptions.push(s2);

    this.friends = friendService.lastFriends;

    friendService.friends.asObservable().subscribe(users => {
      _this.friends = users;
    });
  }
  ngOnInit() {
    this.getGames();
  }

  removeLobby() {
    this.dispatcher.sendMessage("lobby/remove", new LobbyChange(this.currentLobby.lobbyId));
    this.router.navigate(["/lobbies"]);
  }
  leaveLobby() {
    this.dispatcher.sendMessage("lobby/leave", new LobbyChange(this.currentLobby.lobbyId));
    this.router.navigate(["/lobbies"]);
    this.dispatcher.sendMessage("game/list", {});
  }
  subscribeGameList() {
    this.dispatcher.subscribeOnPersonalTopic("game/list", (x) => {
      this.gameList = JSON.parse(x);
      this.logger.log("GAME LIST: arrived", 1, LogType.log, this);
    });
  }
  getGames() {
    this.logger.log("GAME LIST:", 1, LogType.log, this);
    this.dispatcher.sendMessage("game/list", {});
  }
  addGame() {
    this.dispatcher.sendMessage('game/create', {o: this.currentLobby.lobbyId});
    this.created = 'Game created';
    this.getGames();
  }
  back() {
    this.router.navigate(["/lobbies"]);
  }
  playerCountChange() {
    this.dispatcher.sendMessage("lobby/update", this.currentLobby);
  }
  lobbyNameChange() {
    this.dispatcher.sendMessage("lobby/update", this.currentLobby);
  }
  onInvite(){
    this.inviteDialogOpen = false;
    this.notifier.showNotification("Invite sent",NotificationSort.notify);
  }
  makePrivate() {
    this.dispatcher.sendMessage("lobby/update", this.currentLobby);
  }

  addFriend(u: User) {
    this.friendService.sendFriendRequest(u);
    this.notifier.showNotification("Friend request sent.",NotificationSort.notify);
  }

  isFriendable(user: User): boolean {
    let isFriendable = true;
    if (user.unoUserId === this.friendService.currentUser.unoUserId) {
      isFriendable = false;
    } else {
      this.friends.forEach(friend => {
        if (friend.unoUserId === user.unoUserId) {
          isFriendable = false;
        }
      });
    }
    return isFriendable;
  }

  history(id: number) {
    log(id.toString());
    this.router.navigate(['/history', id]);
  }
}
class Games {
  public games: Game[] = [];
}
