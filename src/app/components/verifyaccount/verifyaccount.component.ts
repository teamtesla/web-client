import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-verifyaccount',
  templateUrl: './verifyaccount.component.html',
  styleUrls: ['./verifyaccount.component.scss']
})
export class VerifyaccountComponent implements OnInit {
  public error = false;
  public success = false;
  private token: string;

  constructor(private userService: UserService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token');
    if (this.token) {
      this.userService.verifyUser(this.token).subscribe((result) => {
        if (result) {
          this.success = true;
        } else {
          this.error = true;
        }
      });
    } else {
      this.error = true;
    }
  }
}
