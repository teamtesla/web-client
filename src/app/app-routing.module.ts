import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './components/register/register.component';
import { HandComponent } from './components/spel/hand/hand.component';
import { LoginComponent } from './components/login/login.component';
import { VerifyaccountComponent } from './components/verifyaccount/verifyaccount.component';
import { LobbiesComponent } from './components/lobbies/lobbies.component';
import { LobbyComponent } from './components/lobby/lobby.component';
import { ChatWindowComponent } from './components/chatwindow/chat-window.component';
import { GameComponent } from './components/spel/game/game.component';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { ChooseColorComponent } from './components/spel/choose-color/choose-color.component';
import { PersonallobbiesComponent } from './components/personallobbies/personallobbies.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import {AccountComponent} from './components/account/account.component';
import {HistoryComponent} from './components/history/history.component';

const routes: Routes = [
  {
    path: 'register',
    component: RegisterComponent,
    data: { title: 'Register account' }
  },
  {
    path: 'account',
    component: AccountComponent,
    data: { title: 'My account' },
    canActivate: [AuthGuard]
  },
  {
    path: 'hand',
    component: HandComponent,
    data: { title: 'Hand' },
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Login account' }
  },
  {
    path: 'lobbies',
    component: LobbiesComponent,
    data: { title: 'lobbies' },
    canActivate: [AuthGuard]
  },
  {
    path: 'mylobbies',
    component: PersonallobbiesComponent,
    data: { title: 'My lobbies' },
    canActivate: [AuthGuard]
  },
  {
    path: 'lobby',
    component: LobbyComponent,
    data: { title: 'lobby' },
    canActivate: [AuthGuard]
  },
  {
    path: 'verifyuser/:token',
    component: VerifyaccountComponent,
    data: { title: 'Verify account' }
  },
  {
    path: 'chatwindow',
    component: ChatWindowComponent,
    data: { title: 'ChatWindow' },
    canActivate: [AuthGuard]
  },
  {
    path: 'game',
    component: GameComponent,
    data: { title: 'Game' },
    canActivate: [AuthGuard]
  },
  {
    path: 'statistics',
    component: StatisticsComponent,
    data: { title: 'Statistics' },
    canActivate: [AuthGuard]
  },
  {
    path: 'notifications',
    component: NotificationsComponent,
    data: { title: 'notifications' },
    canActivate: [AuthGuard]
  },
  {
    path: 'choosecolor',
    component: ChooseColorComponent,
    data: { title: 'Statistics' },
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: LobbiesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'history/:id',
    component: HistoryComponent,
    data: { title: 'History' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


}
