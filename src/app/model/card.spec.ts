import {Card} from './card';

describe('Card', () => {
  it('should create a Wild Draw 4 card', () => {
    expect(new Card('black', 'takefour', null)).toBeTruthy();
  });

  it('should create a Wild Color card', () => {
    expect(new Card('black', 'change', null)).toBeTruthy();
  });

  it('should create a Yellow Draw 2 card', () => {
    expect(new Card('yellow', 'taketwo', null)).toBeTruthy();
  });

  it('should create a Blue Skip card', () => {
    expect(new Card('blue', 'skip', null)).toBeTruthy();
  });

  it('should create a Red Reverse card', () => {
    expect(new Card('red', 'Reverse', null)).toBeTruthy();
  });

  it('should create a Green 6 card', () => {
    expect(new Card('green', 'six', null)).toBeTruthy();
  });
});
