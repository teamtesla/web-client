import { Lobby } from './lobby';
import {Game} from './game';

export class User {
  id: string;
  unoUserId: number;
  username: string;
  password: string;
  email: string;
  verified: boolean;
  provider: string;
  providerId: string;
  uuid: string;
  alive: boolean;
  currentLobby: Lobby;
  currentGame: Game;
  friends: User[];
  lobbies: Lobby[];
  hostLobbies: Lobby[];

  constructor(username: string, password: string, email: string) {
    this.username = username;
    this.password = password;
    this.email = email;
  }
}
