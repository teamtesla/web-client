import {Card} from './card';
import {Player} from './player';


export class Game {
  public gameId: number;
  public players;
  public topCard;
  public lobby: number
  public color: string;

  constructor(gameId: number, players, topCard) {
    this.gameId = gameId;
    this.players = players;
    this.topCard = topCard;
  }

}
