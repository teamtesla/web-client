import { User } from './user';

export class Invite {
    receivernames:string[];
    lobby:number;
    sender:User;
    constructor(){
        this.receivernames = new Array();
    }
}
