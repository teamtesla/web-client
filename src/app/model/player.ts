import {Card} from './card';

export class Player {
  public userId: number;
  public username: string;
  public amount: number;
  public cards: Card[];
  public turn: boolean;

  constructor(userId: number, amount: number, cards: Card[], turn: boolean, username: string) {
    this.userId = userId;
    this.amount = amount;
    this.cards = cards;
    this.turn = turn;
    this.username = username;
  }
}
