import {User} from './user';
import {Lobby} from './lobby';

export class ChatMessage {
  id: number;
  message: string;
  sender: User;
  lobby: Lobby;


  constructor(message: string, sender: User, lobby: Lobby) {
    this.message = message;
    this.sender = sender;
    this.lobby = lobby;
  }
}
