import {User} from './user';

export class UserStatistics {
  statsId: number;
  user: User;
  gamesPlayed: number;
  gamesWon: number;
  pointsEarned: number;
  maximumPoints: number;
  averagePoints: number;

  constructor() {

  }

  // getWinRate(): number {
  //   return Math.round(this.wins / this.games * 10000) / 100;
  // }
}
