export enum Color {
  Blue,
  Green,
  Red,
  Yellow,
  Wild
}

export class Card {
  id: number;
  card: string;
  number: number;
  color: Color;
  image: string;
  cardId: number;
  score: number;
  ranrot: number;
  ranX: number;
  ranY: number;
  choiceColor: string;

  constructor(color: string, value: string, id: number) {
    let val: string;
    value = value.toUpperCase();
    switch (value) {
      case 'ZERO':
        val = '0';
        break;
      case 'ONE':
        val = '1';
        break;
      case 'TWO':
        val = '2';
        break;
      case 'THREE':
        val = '3';
        break;
      case 'FOUR':
        val = '4';
        break;
      case 'FIVE':
        val = '5';
        break;
      case 'SIX':
        val = '6';
        break;
      case 'SEVEN':
        val = '7';
        break;
      case 'EIGHT':
        val = '8';
        break;
      case 'NINE':
        val = '9';
        break;
      case 'REVERSE':
        val = 'R';
        break;
      case 'SKIP':
        val = 'S';
        break;
      case 'TAKETWO':
        val = 'D2';
        break;
      case 'CHANGE':
        val = 'C';
        break;
      case 'TAKEFOUR':
        val = 'D4';
        break;
    }
    this.cardId = id;
    if (color.toString() === 'BLACK') {
      this.assignCard('W' + val);
    } else {
      this.assignCard(color.toString().charAt(0) + val);
    }
  }

  private assignCard(image: string) {
    this.image = image;
    const color = image.charAt(0);
    switch (color) {
      case 'B':
        this.color = Color.Blue;
        break;
      case 'G':
        this.color = Color.Green;
        break;
      case 'R':
        this.color = Color.Red;
        break;
      case 'Y':
        this.color = Color.Yellow;
        break;
      case 'W':
        this.color = Color.Wild;
        break;
    }

    const card = image.substr(1);
    switch (card) {
      case '0':
        this.card = '0';
        this.number = 0;
        this.score = 0;
        break;
      case '1':
        this.card = '1';
        this.number = 1;
        this.score = 1;
        break;
      case '2':
        this.card = '2';
        this.number = 2;
        this.score = 2;
        break;
      case '3':
        this.card = '3';
        this.number = 3;
        this.score = 3;
        break;
      case '4':
        this.card = '4';
        this.number = 4;
        this.score = 4;
        break;
      case '5':
        this.card = '5';
        this.number = 5;
        this.score = 5;
        break;
      case '6':
        this.card = '6';
        this.number = 6;
        this.score = 6;
        break;
      case '7':
        this.card = '7';
        this.number = 7;
        this.score = 7;
        break;
      case '8':
        this.card = '8';
        this.number = 8;
        this.score = 8;
        break;
      case '9':
        this.card = '9';
        this.number = 9;
        this.score = 9;
        break;
      case 'R':
        this.card = 'reverse';
        this.number = 10;
        this.score = 20;
        break;
      case 'S':
        this.card = 'skip';
        this.number = 11;
        this.score = 20;
        break;
      case 'D2':
        this.card = 'draw 2';
        this.number = 12;
        this.score = 20;
        break;
      case 'C':
        this.card = 'color';
        this.number = 13;
        this.score = 50;
        break;
      case 'D4':
        this.card = 'draw 4';
        this.number = 14;
        this.score = 50;
        break;
    }
  }
}
