import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { UserService } from './services/user.service';
import { LobbyService } from './services/lobby.service';
import { WebsocketDispatcherService } from './services/websocket-dispatcher.service';
import { UserStateService } from './services/user-state.service';
import { NotificationService, NotificationSort } from './services/notification.service';
import { timeout } from 'q';
import { GameService } from './services/game.service';
import {FriendService} from './services/friend.service';
import { CurrentUserService } from './services/current-user.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  title = 'Uno';
  notification = "test";
  notifications: NotificationShowItem[] = new Array();
  constructor(
    private userService: UserService,
    private lobbyService: LobbyService,
    private userStateService: UserStateService,
    private dispatcher: WebsocketDispatcherService,
    private notifier: NotificationService,
    private gameService: GameService,
    private friendService : FriendService,
    private currentUserService : CurrentUserService) {

  }
  ngAfterViewInit(): void {
    this.doSubscriptions();
    this.userService.checkLoggedIn();
  }
  logout() {
    this.userService.doLogout();
  }

  isLoggedin(): boolean {
    return this.userService.isLoggedin();
  }

  doSubscriptions() {
    this.lobbyService.Start();
    this.lobbyService.StartInvitationListeners();
    this.gameService.start();
    this.currentUserService.start();
    this.friendService.doSubscriptions();
    this.userStateService.start();
    var _this = this;
    this.notifier.getNotifier().subscribe((nbar) => {
      var nsi = new NotificationShowItem();
      if (nbar.sort == NotificationSort.notify) nsi.color = "rgb(107, 171, 109)";
      if (nbar.sort == NotificationSort.warn) nsi.color = "orange";
      if (nbar.sort == NotificationSort.error) nsi.color = "red";
      nsi.message = nbar.message;
      _this.notifications.push(nsi);
      setTimeout(() => {
        _this.removeNotification(nsi, _this);
      }, 3000);
    });
  }
  removeNotification(x, _this) {
    const index = _this.notifications.indexOf(x);

    if (index >= 0) {
      _this.notifications.splice(index, 1);
    }
  }
  getNotificationNumber(): number {
    return this.lobbyService.lastLobbyInvites.length + this.friendService.lastFriendRequests.length;
  }
}
class NotificationShowItem {
  color: string;
  message: string;
}

