import {TestBed} from '@angular/core/testing';
import {WebsocketDispatcherService} from './websocket-dispatcher.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {StorageServiceModule} from 'angular-webstorage-service';

describe('WebsocketDispatcherService', () => {
  beforeEach(() => TestBed.configureTestingModule( {
    imports: [
      HttpClientTestingModule,
      StorageServiceModule
    ]
  }));

  it('should be created', () => {
    expect(TestBed.get(WebsocketDispatcherService)).toBeTruthy();
  });
});
