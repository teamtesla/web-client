import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  notifier : Subject<NotificationBar> = new Subject();
  constructor() { }
  showNotification(message:string, sort:NotificationSort){
    var notbar:NotificationBar = {message:message, sort:sort};
    this.notifier.next(notbar);
  }
  getNotifier(){
    return this.notifier.asObservable();
  }
}
export enum NotificationSort{
  notify,
  warn,
  error
}
export class NotificationBar{
  message:string;
  sort:NotificationSort
}
