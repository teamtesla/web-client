import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { Subject } from 'rxjs';
import { WebsocketDispatcherService } from './websocket-dispatcher.service';
import { NotificationService, NotificationSort } from './notification.service';
import {Debugger} from 'inspector';
import {log} from 'util';
import {LogService, LogType} from './log.service';

@Injectable({
  providedIn: 'root'
})
export class FriendService {
  public currentUser: User = new User("loading","","");

  lastFriends: User[] = [];
  friends: Subject<User[]> = new Subject();
  lastFriendRequests: FriendRequest[] = [];
  friendRequests: Subject<FriendRequest[]> = new Subject();
  constructor(private dispatcher: WebsocketDispatcherService, private notifier: NotificationService) {
  }
  doSubscriptions() {
    this.subscribeGetUser();
    this.getCurrentUser();

    const _this = this;
    this.dispatcher.subscribeOnPersonalTopic('friend/list', (x) => {
      const fl: FriendList = JSON.parse(x);
      _this.lastFriends = fl.friendlist;
      _this.friends.next(fl.friendlist);
    });

    this.dispatcher.subscribeOnPersonalTopic('friend/request/list', (x) => {
      const fRL: FrienRequestList = JSON.parse(x);
      _this.lastFriendRequests = fRL.friendRequests;
      _this.friendRequests.next(fRL.friendRequests);
    });

    this.dispatcher.subscribeOnPersonalTopic('friend/request/new', (x) => {
      const friendRequest: FriendRequest = JSON.parse(x);
      _this.lastFriendRequests.push(friendRequest);
      _this.friendRequests.next(_this.lastFriendRequests);
      _this.notifier.showNotification('You have a friend request from ' + friendRequest.sender, NotificationSort.notify);
    });
    this.getFriendRequests();
    this.getFriends();
  }

  sendFriendRequest(user: User) {
    const request : FriendRequest = new FriendRequest();
    request.receiver = user.email;
    this.dispatcher.sendMessage('friend/request/make', request);
  }
  acceptFriendRequest(friendRequest: FriendRequest) {
    this.lastFriendRequests.slice(this.lastFriendRequests.indexOf(friendRequest), 1);
    this.dispatcher.sendMessage('friend/request/accept', friendRequest);
  }
  declineFriendRequest(friendRequest: FriendRequest) {
    this.lastFriendRequests.slice(this.lastFriendRequests.indexOf(friendRequest), 1);
    this.dispatcher.sendMessage('friend/request/decline', friendRequest);
  }
  getFriendRequests() {
    this.dispatcher.sendMessage('friend/request/list', new FrienRequestList());
  }
  getFriends() {
    this.dispatcher.sendMessage('friend/list', new FriendList());
  }

  
  subscribeGetUser() {
    this.dispatcher.subscribeOnPersonalTopic('user/info', (x) => {
      this.currentUser = JSON.parse(x);
      console.log(x);
    });
  }

  getCurrentUser() {
    this.dispatcher.sendMessage('user/info', {});
  }
  
}

class FriendList {
  friendlist: User[];
}
export class FriendRequest {
  sender: string;
  receiver: string;
  friendRequestId: number;
}

class FrienRequestList {
  friendRequests: FriendRequest[];
}
