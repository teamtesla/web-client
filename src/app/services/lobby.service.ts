import { Injectable } from '@angular/core';
import { Lobby } from '../model/lobby';
import { WebsocketDispatcherService } from './websocket-dispatcher.service';
import { Subject } from 'rxjs';
import { LogService, LogType } from './log.service';
import { Invite } from '../model/invite';
import { NotificationService, NotificationSort } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class LobbyService {

  currentLobby : Subject<Lobby> = new Subject();
  lastCurrentLobby : Lobby;
  lobbyList : Subject<Lobby[]> = new Subject();
  lastLobbyList : Lobby[] = new Array();
  lastLobbyInvites : Invite[] = new Array();
  lobbyInvites : Subject<Invite[]> = new Subject;

  constructor(
    private dispatcher : WebsocketDispatcherService,
    private logger:LogService,
    private notifier: NotificationService
    ) {
   }

   //Start main lobby mqtt listeners 
   public Start(){
     //change current lobby when server sais so
    this.dispatcher.subscribeOnPersonalTopic("lobby/current",(c) =>{
      this.currentLobby.next(JSON.parse(c));
      this.lastCurrentLobby = JSON.parse(c);
      this.logger.log("[LOBBY SERVICE] lobby object arrived",3,LogType.log,this);
    })

    //change list of lobbies when server sends one [PUBLIC]
    this.dispatcher.subscribeOnTopic("lobby/list",(c) =>{
      this.logger.log("[LOBBY SERVICE] TEST",1,LogType.log,this);
      this.lobbyList.next(JSON.parse(c));
      this.lastLobbyList = JSON.parse(c);
      this.logger.log("[LOBBY SERVICE] lobby list object arrived",1,LogType.log,this);
    })

    //change list of lobbies when server sends one [PERSONAL]
    this.dispatcher.subscribeOnPersonalTopic("lobby/list",(c) =>{
      this.logger.log("[LOBBY SERVICE] TEST",3,LogType.log,this);
      this.lobbyList.next(JSON.parse(c));
      this.lastLobbyList = JSON.parse(c);
      this.logger.log("[LOBBY SERVICE] lobby list object arrived",3,LogType.log,this);
    })

    //update current lobby when server sends a change
    this.dispatcher.subscribeOnPersonalTopic("lobby/updateCurrent",(c) =>{
      var lobby: Lobby = JSON.parse(c);
      if(this.lastCurrentLobby.lobbyId == lobby.lobbyId){
        this.currentLobby.next(lobby);
        this.lastCurrentLobby = lobby;
        this.logger.log("[LOBBY SERVICE] lobby update arrived",3,LogType.log,this);
      }
    })

    //ask server to send new list of lobbies
    this.dispatcher.sendMessage("lobby/list",new Object());
    //ask server to send current lobby
    this.dispatcher.sendMessage("lobby/get",new Object());

    this.logger.log("[LOBBY SERVICE] get lobby list",3,LogType.log,this);
    this.logger.log("main started",1,LogType.good,this);
   }

   //start lobby invitation mqtt listeners
   StartInvitationListeners(){
    this.logger.log("invitation listeners started",1,LogType.good,this);

    //update invitation list when server sends one
    var _this = this;
    this.dispatcher.subscribeOnPersonalTopic("invitation/list",(c)=>{
      _this.lastLobbyInvites = JSON.parse(c);
      _this.lobbyInvites.next(JSON.parse(c));
      _this.logger.log("invitation list arrived",1,LogType.log,this);
    })

    //process incomming lobby invite
    this.dispatcher.subscribeOnPersonalTopic("invitation/new",(x)=>{
      var invite : Invite = JSON.parse(x);
      _this.lastLobbyInvites.push(invite);
      _this.lobbyInvites.next();
      _this.notifier.showNotification("You've got a lobby invite from " + invite.sender.username,NotificationSort.notify);
      _this.logger.log("invitation arrived",1,LogType.log,this);
      

    })

    //ask server to send invitation list
    this.dispatcher.sendMessage("invitation/list",new Object());
   }


   currentLobbyObservable(){
     return this.currentLobby.asObservable();
   }
   lobbyListObservable(){
     return this.lobbyList.asObservable();
   }
   getLastLobbyList(){
     return this.lastLobbyList;
   }
   getLastCurrentLobby(){
     return this.lastCurrentLobby;
   }
}
