import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {LOCAL_STORAGE, StorageService} from 'angular-webstorage-service';
import {User} from '../model/user';
import { LogService, LogType } from './log.service';

const endpoint = environment.api_url + 'auth/';
let errorMessage = '';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': '*'
    })
  };

  private STORAGE_KEY_TOKEN = 'jwtToken';
  private STORAGE_KEY_EXPIRES = 'jwtExpires';
  private STORAGE_KEY_UUID = 'uuidToken';

  constructor(private http: HttpClient, @Inject(LOCAL_STORAGE) private storage: StorageService,private logger : LogService) {

  }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  getUsers(): Observable<any> {
    return this.http.get(endpoint + 'users').pipe(
      map(this.extractData));
  }

  getFromServerUuid(): Observable<any> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': '*',
        'Authorization': 'Bearer ' + this.getJwtToken()
      })
    };
    return this.http.get(endpoint + 'getUUID', this.httpOptions).pipe(
      map(this.extractData));
  }

  getUser(id): Observable<any> {
    return this.http.get(endpoint + 'users/' + id).pipe(
      map(this.extractData));
  }

  addUser(user): Observable<any> {
    return this.http.post<any>(endpoint + 'signup', JSON.stringify({
      'username': user.username,
      'email': user.email,
      'password': user.password
    }), this.httpOptions).pipe(
      tap((u) => console.log(`added user w/ id=${u.id}`)),
      catchError(this.handleError<any>('addUser'))
    );
  }

  get getErrorMessage(): string {
    return errorMessage;
  }

  updateUser(user): Observable<any> {
    return this.http.put(endpoint + 'users/' + user.id, JSON.stringify(user), this.httpOptions).pipe(
      tap(_ => console.log(`updated user id=${user.id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  deleteUser(id): Observable<any> {
    return this.http.delete<any>(endpoint + 'users/' + id, this.httpOptions).pipe(
      tap(_ => console.log(`deleted user id=${id}`)),
      catchError(this.handleError<any>('deleteUser'))
    );
  }

  getJwtToken(): string | undefined {
    if (this.storage.get(this.STORAGE_KEY_TOKEN)) {
      return this.storage.get(this.STORAGE_KEY_TOKEN);
    }
  }

  setJwtToken(token: string): void {
    this.storage.set(this.STORAGE_KEY_TOKEN, token);
  }

  setUuid(uuid: string): void {
    this.storage.set(this.STORAGE_KEY_UUID, uuid);
  }

  getUuid(): string | undefined {
    if (this.storage.get(this.STORAGE_KEY_UUID)) {
      return this.storage.get(this.STORAGE_KEY_UUID);
    }
  }

  getAuthenticationHeader(): any {
    const token = this.getJwtToken();
    if (token) {
      return {'Authorization': 'Bearer ' + token};
    }
  }

  verifyUser(token: string): Observable<any> {
    return this.http.post<any>(endpoint + 'registrationConfirm', token, this.httpOptions).pipe(
      tap((result) => this.setJwtToken(result.accessToken)),
      catchError(this.handleError<any>('verifyUser')));
  }

  doLogin(email: string, password: string): Observable<any> {
    return this.http.post<any>(endpoint + 'login', JSON.stringify({'email': email, 'password': password}), this.httpOptions).pipe(
      tap((result) => {
        this.setJwtToken(result.accessToken);
        
      }),
      catchError(this.handleError<any>('loginUser'))
    );
  }


  doLogout(): void {
    this.storage.remove(this.STORAGE_KEY_TOKEN);
  }

  verifyToken(): Observable<any> {
    const httpOptionsWithLogin = this.httpOptions;
    httpOptionsWithLogin.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': '*',
      'Authorization': 'Bearer ' + this.getJwtToken()
    });
    // console.log(httpOptionsWithLogin);
    return this.http.post<any>(endpoint + 'verifytoken', 'verifyToken', httpOptionsWithLogin).pipe(
      tap((result) => result),
      catchError(this.handleError<any>('verifyToken')));
  }

  checkLoggedIn(): void {
    if (this.getJwtToken()) {
      this.verifyToken().subscribe((result) => {
        if (result) {
          this.setLoggedinHeaders();
        } else {
          this.doLogout();
        }
      });
    }
  }

  setLoggedinHeaders(): void {
    this.httpOptions.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': '*',
      'Authorization': 'Bearer ' + this.getJwtToken()
    });
  }

  isLoggedin(): boolean {
    return !!this.getJwtToken();
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      if (error.message.toString().includes('400')) {
        errorMessage = 'This email already exists!';
      } else if (error.message.toString().includes('login')) {
        errorMessage = 'The account details are wrong!';
      } else {
        errorMessage = 'Something went wrong. Please try again later!';
      }
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
