import {Injectable} from '@angular/core';
import {Client, ConnectionOptions, Message} from 'paho-mqtt';
import {UserService} from './user.service';
import {LogService, LogType} from './log.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class WebsocketDispatcherService {

  public ConnectionStatus: boolean;
  private TopicCallbacks: TopicCallback[] = new Array();
  private client;
  private userService: UserService;
  public isConnected = false;

  constructor(userService: UserService, private logger: LogService) {
    this.ClientInit();
    this.userService = userService;

  }

  // CLIENT INIT
  ClientInit() {
    this.client = new Client(environment.brokerName, Number(environment.brokerPort), this.uuidv4());
    this.client.onMessageArrived = (message) => this.onMessageArrived(message, this);
    this.client.onConnectionLost = this.onConnectionLost;
    const options: ConnectionOptions = {};
    options.onSuccess = () => this.onConnect();
    options.onFailure = this.onConnectFailed;
    this.client.connect(options);

  }

  // GENERATE (FAKE) UUID
  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  onConnect() {
    this.logger.log('connected', 1, LogType.good, this);
    this.isConnected = true;
  }

  onConnectFailed() {
    this.logger.log('connection failed', 1, LogType.error, this);
    this.isConnected = false;
  }

  disconnect() {

  }

  subscribeOnTopic(_topic, callback) {

    const topic = 'user/' + _topic;
    const _this = this;
    const interval = setInterval(
      function () {
        if (_this.isConnected) {
          let exists: Boolean = false;
          _this.TopicCallbacks.forEach(tc => {
            if (tc.topic === topic) {
              tc.callbacks.push(callback);
              exists = true;
              _this.logger.log('[LOBBY SERVICE] callback added', 2, LogType.log, _this);
            }
          });
          if (!exists) {
            const tc: TopicCallback = new TopicCallback();
            tc.callbacks.push(callback);
            tc.topic = topic;
            _this.TopicCallbacks.push(tc);
            _this.logger.log('[LOBBY SERVICE] callback added', 2, LogType.log, _this);
          }
          _this.client.subscribe(topic);
          _this.logger.log('[SUBSCRIBE] ' + topic, 2, LogType.log, _this);

          clearInterval(interval);
        } else {
          _this.logger.log('Awaiting connection...' + topic, 1, LogType.log, _this);
        }
      }, 1000
    );


  }


  subscribeOnPersonalTopic(topic, callback) {
    this.subscribeOnTopic(this.userService.getUuid() + '/' + topic, callback);
  }

  sendMessage(topic: string, o: object) {

    const _this = this;
    const interval = setInterval(
      function () {
        if (_this.isConnected) {
          _this.logger.log('[OBJECT SEND] ' + JSON.stringify(o) + topic, 4, LogType.log, _this);
          const websocketBaseMessage: WebsocketBaseMessage = new WebsocketBaseMessage(_this.userService.getJwtToken(), JSON.stringify(o));
          const message: Message = new Message(JSON.stringify(websocketBaseMessage));
          message.destinationName = topic;
          message.qos = 2;
          _this.client.send(message);
          _this.logger.log('[MESSAGE SEND] ' + JSON.stringify(message), 4, LogType.log, _this);

          clearInterval(interval);
        } else {
          _this.logger.log('Awaiting connection...', 1, LogType.log, _this);
        }
      }, 1000
    );

  }

  // TODO: Unsubscribe

  private onMessageArrived(message: MQTTObject, _this) {
    this.logger.log('[MESSAGE ARRIVED] ' + message.topic, 3, LogType.log, this);
    this.logger.log(message.payloadString, 4, LogType.log, this);
    _this.TopicCallbacks.forEach(tc => {
      _this.logger.log(tc.topic + '   |   ' + message.topic, 4, LogType.log, _this);
      if (tc.topic === message.topic) {
        try {
          tc.callbacks.forEach(m => m(message.payloadString));
        } catch (ex) {
          _this.logger.log(ex.message, 1, LogType.error);
        }
      }
    });

  }

  private onConnectionLost(message) {
    this.isConnected = false;
  }

}

export class TopicCallback {
  topic: string;
  callbacks: any[];

  constructor() {
    this.callbacks = new Array();
  }
}

export class MQTTObject {
  payloadString: string;
  payloadBytes: string;
  destinationName: string;
  qos: number;
  retained: boolean;
  topic: string;
  duplicate: boolean;
}

export class WebsocketBaseMessage {
  token: string;
  object: string;

  constructor(token: string, o: string) {
    this.token = token;
    this.object = o;
  }
}
