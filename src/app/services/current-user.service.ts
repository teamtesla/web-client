import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { User } from '../../app/model/user';
import { WebsocketDispatcherService } from './websocket-dispatcher.service';
import { LogService, LogType } from './log.service';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserService {

  public lastCurrentUser;
  public currentUser : Subject<User> = new Subject();
  constructor(private dispatcher:WebsocketDispatcherService, private logger : LogService) { }
  public start(){
    var _this = this;
    this.dispatcher.subscribeOnPersonalTopic("user/info",(x)=>{
      _this.lastCurrentUser = JSON.parse(x);
      _this.currentUser.next(JSON.parse(x));
      _this.logger.log("current user received",2,LogType.log,_this);
    });
    this.dispatcher.sendMessage("user/info",new Object());
    _this.logger.log("started",1,LogType.good,_this);
  }

}
