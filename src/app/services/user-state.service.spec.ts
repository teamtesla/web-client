import { TestBed } from '@angular/core/testing';

import { UserStateService } from './user-state.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {StorageServiceModule} from 'angular-webstorage-service';

describe('UserStateService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
      StorageServiceModule
    ]
  }));

  it('should be created', () => {
    expect(TestBed.get(UserStateService)).toBeTruthy();
  });
});
