import { Injectable } from '@angular/core';
import {WebsocketDispatcherService} from './websocket-dispatcher.service';
import {Game} from '../model/game';
import {Observable, Subject} from 'rxjs';
import {Turn} from '../model/turn';
import { LogService, LogType } from './log.service';
import { LobbyService } from './lobby.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private game: Subject<Game> = new Subject();

  constructor(
    private dispatcher: WebsocketDispatcherService,
    private logger:LogService,
    private lobbyService:LobbyService,
    private router : Router) { }

  public start() {
    var _this = this;
    this.dispatcher.subscribeOnPersonalTopic('game/turn', (c) => {
        _this.game.next(JSON.parse(c));
    });
    this.dispatcher.subscribeOnPersonalTopic('game/current', (c) => {
      _this.game.next(JSON.parse(c));
    });

    this.dispatcher.subscribeOnPersonalTopic('game/start', (c) =>{
      var game :Game = JSON.parse(c);
      _this.logger.log("game started",1,LogType.log,this);

      if(_this.lobbyService.lastCurrentLobby.lobbyId == game.lobby){
        _this.router.navigate(["/game"]);
      }
    })
    this.logger.log("started",1,LogType.good,this);

  }

  public sendGameData(gameId: number, cardId: number, userId: number, uno: boolean) {
    const turn = new Turn();
    turn.gameId = gameId;
    turn.cardId = cardId;
    turn.userId = userId;
    turn.uno = uno;
    this.dispatcher.sendMessage('game/turn', turn);
  }

  public sendGameDataWithColor(gameId: number, cardId: number, userId: number, uno: boolean, color: string) {
    const turn = new Turn();
    turn.gameId = gameId;
    turn.cardId = cardId;
    turn.userId = userId;
    turn.uno = uno;
    turn.choiceColor = color;
    this.dispatcher.sendMessage('game/turn', turn);
  }

  public initializeGame() {
    this.dispatcher.sendMessage('game/get', {});
  }

  public getGameObservable() {
    return this.game.asObservable();
  }
}
