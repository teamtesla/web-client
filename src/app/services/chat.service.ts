import { Injectable } from '@angular/core';
import {ChatMessage} from '../model/chat-message';
import {Subject} from 'rxjs';
import {WebsocketDispatcherService} from './websocket-dispatcher.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  message: Subject<ChatMessage> = new Subject();

  constructor(private dispatcher : WebsocketDispatcherService) { }

  start(){
    var _this = this;
    this.dispatcher.subscribeOnPersonalTopic("chat/message",function(x){
      console.log("[MESSAGE SERVICE] message arrived: " + JSON.stringify(x));
      var cm: ChatMessage = JSON.parse(x);
      _this.message.next(cm);
    });
  }

  sendMessage(cm: ChatMessage){
    this.dispatcher.sendMessage("chat/message",cm);
  }
}
