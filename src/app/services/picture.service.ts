import {Inject, Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {LOCAL_STORAGE, StorageService} from 'angular-webstorage-service';
import {Observable, Subject} from 'rxjs';
import {publicDecrypt} from 'crypto';
import {UserService} from './user.service';


const endpoint = environment.api_url + 'userDetails/';
const errorMessage = '';


@Injectable({
  providedIn: 'root'
})
export class PictureService {
  // Headers 2
  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': '*',
      'Authorization': 'Bearer ' + this.userService.getJwtToken()
    })
  };

  constructor(private http: HttpClient, @Inject(LOCAL_STORAGE) private storage: StorageService, private userService: UserService) { }

  setProfilePicture(picture: File) {
    const fd = new FormData();
    fd.append('file', picture);
    return this.http.post<any>(endpoint + 'upload', fd, this.httpOptions).subscribe(result => {console.log('Call Succeeded'); });
    // return this.http.post<any>(endpoint + 'upload', fd , this.httpOptions);
  }

  getProfilePicture(id: number) : Observable<any> {
    return this.http.get<any>(endpoint + 'profilePic/' + id);
  }
}
