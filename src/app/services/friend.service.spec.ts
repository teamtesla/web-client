import { TestBed } from '@angular/core/testing';

import { FriendService } from './friend.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {StorageServiceModule} from 'angular-webstorage-service';

describe('FriendService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
      StorageServiceModule
    ]
  }));

  it('should be created', () => {
    const service: FriendService = TestBed.get(FriendService);
    expect(service).toBeTruthy();
  });
});
