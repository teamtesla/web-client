import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { Subject } from 'rxjs';
import { WebsocketDispatcherService } from './websocket-dispatcher.service';
import { LogService, LogType } from './log.service';

@Injectable({
  providedIn: 'root'
})
export class UserStateService {

  userStateChanger : Subject<User> = new Subject();
  unoUsers:User[] = new Array();
   
  
  constructor(private dispatcher : WebsocketDispatcherService, private logger :LogService) {
   
   }
  start(){
    var _this = this;

    this.dispatcher.subscribeOnPersonalTopic("userStateChange",c =>{
      _this.logger.log("OBJECT RECEIVED: " + JSON.parse(c),4,LogType.log,_this);
      var unoUser : User = JSON.parse(c);
      _this.userStateChanger.next(unoUser);
      var exists = false;
      _this.unoUsers.forEach(user=>{if(user.id == unoUser.id) user.alive = unoUser.alive; exists = true; });
      if(!exists) this.unoUsers.push(unoUser);
      _this.logger.log(JSON.parse(c),4,LogType.log,_this);
    })

    this.dispatcher.subscribeOnPersonalTopic("ping",c=>{
      _this.dispatcher.sendMessage("user/state/alive",new Object());
    })

    this.dispatcher.subscribeOnPersonalTopic("user/state/get",c=>{
      _this.logger.log("OBJECT RECEIVED: " + JSON.stringify(c),4,LogType.log,_this);
      _this.unoUsers = JSON.parse(c);
      this.unoUsers.forEach(u =>{
        this.userStateChanger.next(u);
      })
      _this.logger.log(JSON.stringify(_this.unoUsers),4,LogType.log,_this);
    })

    this.dispatcher.sendMessage("user/state/get",new Object());

    this.logger.log("started",1,LogType.good,this);
  }
  userStateChangerObservable(){
    return this.userStateChanger.asObservable();
  }
  isAlive(user : User):boolean{
    var rbool = false;
    this.unoUsers.forEach(u=>{
      if(u.unoUserId == user.unoUserId) return rbool = u.alive;
    });
    return rbool;
  }
}
