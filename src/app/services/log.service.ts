import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  maxLogLevel = 1;
  constructor() {
   }

   public log(message:string, level:number, logtype:LogType, _this){
     if(level <= this.maxLogLevel){
      var className: string = _this.constructor.toString().match(/\w+/g)[1];
       var _message = "[" + className + "]  " + message;
       if(logtype == LogType.log) console.log('%c [' + className +'] %c' + message, 'background: transparant; color: blue; font-weight:bold', 'background: transparant; color: LightSkyBlue ;');
       if(logtype == LogType.good) console.log('%c [' + className +'] %c' + message, 'background: transparant; color: green; font-weight:bold', 'background: transparant; color: #9dd69c ;');
       if(logtype == LogType.special) console.log('%c [' + className +'] %c' + message, 'background: transparant; color: purple; font-weight:bold', 'background: transparant; color: purple;');
       if(logtype == LogType.warn) console.warn(_message);
       if(logtype == LogType.error) console.error(_message);
     }
   }
}
export enum LogType{
  good,
  special,
  log,
  warn,
  error
}

