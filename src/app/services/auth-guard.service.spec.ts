import { TestBed } from '@angular/core/testing';

import { AuthGuardService } from './auth-guard.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {UserService} from './user.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {StorageServiceModule} from 'angular-webstorage-service';

describe('AuthGuardService', () => {

  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientModule, StorageServiceModule ],
    schemas: [
      NO_ERRORS_SCHEMA
    ]
  }));

  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service).toBeTruthy();
  });
});
