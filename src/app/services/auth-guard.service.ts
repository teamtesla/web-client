import { Injectable } from '@angular/core';
import {UserService} from './user.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private userService: UserService, private router: Router) { }

  public canActivate(): boolean {
    if (this.userService.isLoggedin()) {
      return true;
    } else {
      this.router.navigate(['/login']);
    }
  }
}
