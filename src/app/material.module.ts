import {NgModule} from '@angular/core';
import {MatInputModule} from '@angular/material/input';

import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatSidenavModule,
  MatListModule,
  MatTabsModule,
  MatFormFieldModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatProgressBarModule,
  MatDialogModule,
  MatCheckboxModule,
  MatChipsModule,
  MatBadgeModule
} from '@angular/material';


@NgModule({
  imports: [
    MatButtonModule,
    MatCheckboxModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatProgressBarModule,
    MatDialogModule,
    MatChipsModule,
    MatBadgeModule 
  ],
  exports: [
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatProgressBarModule,
    MatDialogModule,
    MatCheckboxModule,
    MatChipsModule,
    MatBadgeModule
  ]
})
export class MaterialModule {
}
