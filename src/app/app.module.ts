import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {HttpClientModule} from '@angular/common/http';
import {StorageServiceModule} from 'angular-webstorage-service';
import {NgScrollbarModule} from 'ngx-scrollbar';
import {UserService} from './services/user.service';
import {AuthGuardService} from './services/auth-guard.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

import {AppComponent} from './app.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RegisterComponent} from './components/register/register.component';
import {LoginComponent} from './components/login/login.component';
import {VerifyaccountComponent} from './components/verifyaccount/verifyaccount.component';
import {LobbiesComponent} from './components/lobbies/lobbies.component';
import {LobbyComponent} from './components/lobby/lobby.component';
import {HandComponent} from './components/spel/hand/hand.component';
import {ChatWindowComponent} from './components/chatwindow/chat-window.component';
import {PlayerComponent} from './components/spel/player/player.component';
import {GameComponent} from './components/spel/game/game.component';
import {KaartComponent} from './components/spel/kaart/kaart.component';
import {StatisticsComponent} from './components/statistics/statistics.component';
import {ChooseColorComponent} from './components/spel/choose-color/choose-color.component';
import { InviteComponent } from './components/invite/invite.component';
import {PersonallobbiesComponent} from './components/personallobbies/personallobbies.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import {resultList, RxSpeechRecognitionService} from '@kamiazya/ngx-speech-recognition';
import {AccountComponent} from './components/account/account.component';
import {AddPictureComponent} from './components/add-picture/add-picture.component';
import { HistoryComponent } from './components/history/history.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    VerifyaccountComponent,
    HandComponent,
    LobbiesComponent,
    PersonallobbiesComponent,
    LobbyComponent,
    ChatWindowComponent,
    GameComponent,
    PlayerComponent,
    KaartComponent,
    StatisticsComponent,
    InviteComponent,
    NotificationsComponent,
    AccountComponent,
    ChooseColorComponent,
    AddPictureComponent,
    HistoryComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    NgScrollbarModule,
    StorageServiceModule
  ],
  providers: [ HttpClientModule, HttpClientTestingModule, RxSpeechRecognitionService ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
